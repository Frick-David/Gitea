This repo holds my docker setup for my personal Gitea server. I am going to self-host the code I want publicly available 
instead of using Gitlab or Github. I will be linking my Gitea server with Authelia as well. I added gitea/ssh to the .gitignore 
to prevent my ssh keys from being uploaded publicly.
